package org.bungeni.editor.gwt.an.server.service.gwt;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bungeni.editor.gwt.an.server.OAuthBungeniConsumerImpl;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("serial")
public class OAuthBungeniAccessTokenServlet extends HttpServlet{

	//@SuppressWarnings("deprecation")
	public void doGet(HttpServletRequest request, HttpServletResponse response){ 
	
		String code = request.getParameter("code");
		HttpSession session = request.getSession();
		//session.putValue("code", code);
		session.setAttribute("code", code);
		OAuthBungeniConsumerImpl oauthC = new OAuthBungeniConsumerImpl();
		String sAccessToken = oauthC.getAccessToken(code);
		
		String accessToken = "";
		try {
			JSONObject jso = new JSONObject(sAccessToken);
			accessToken = (String) jso.get("access_token");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		oauthC.getWorkspaceSections(accessToken);
        oauthC.getWorkspaceFilteredInboxByStatus(accessToken, "received");
	}
}
