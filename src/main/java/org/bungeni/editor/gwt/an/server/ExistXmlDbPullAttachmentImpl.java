package org.bungeni.editor.gwt.an.server;

public class ExistXmlDbPullAttachmentImpl {

	private RequestBuilder rBuilder;

	final String url = "http://127.0.0.1:8088/exist/restxq/attachment";
	
	public ExistXmlDbPullAttachmentImpl() {
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
	}
		
	public ExistXmlDbPullAttachmentImpl(String name) {
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
		setDocumentName(name);
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setDocumentName(String name){
		
		rBuilder.addParameter("name=".concat(name));
	}
	
	public String pull(){
		
		try{
			//if(documentName.isEmpty())
				//throw new Exception("Document name not specified!");
			rBuilder.doRequest();
			String response = rBuilder.getResponse();
			System.out.println("--Success--");
			return response;
		}
		catch(Exception e){
			
			System.out.println(e.getMessage());
			return null;
		}
	}
}
