package org.bungeni.editor.gwt.an.server;

public class ExistXmlDbPopAmendmentImpl {

	private RequestBuilder rBuilder;

	final String url = "http://localhost:8088/exist/restxq/amendment/remove";
	final String defaultMethod = "DELETE";
	
	String username;
	String password;
	String filename;
	
	public ExistXmlDbPopAmendmentImpl() {
		
		rBuilder = new RequestBuilder();
		//rBuilder.setServiceUrl(url);
		rBuilder.setMethod(defaultMethod);
	}
		
	public ExistXmlDbPopAmendmentImpl(String name) {
		
		rBuilder = new RequestBuilder();
		//rBuilder.setServiceUrl(url);
		rBuilder.setMethod(defaultMethod);
		setFilename(name);
	}
	
	public ExistXmlDbPopAmendmentImpl(String name, String method) {
		
		rBuilder = new RequestBuilder();
		//rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
		setFilename(name);
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setUsername(String username){
		
		this.username=username;
	}
	
	public void setPassword(String password){
		
		this.password=password;
	}
	
	public void setFilename(String filename){
		
		this.filename=filename;
	}
	
	public String pop(){
		
		try{
			//if(documentName.isEmpty())
				//throw new Exception("Document name not specified!");
			StringBuilder sb = new StringBuilder();
			//sb.s
			sb.append(url.concat("/"));
			sb.append(username.concat("/"));
			sb.append(password.concat("/"));
			sb.append(filename);
			
			rBuilder.setServiceUrl(sb.toString());
			rBuilder.doRequest();
			String response = rBuilder.getResponse();
			System.out.println("--Success--");
			return response;
		}
		catch(Exception e){
			
			System.out.println(e.getMessage());
			return null;
		}
	}
}
