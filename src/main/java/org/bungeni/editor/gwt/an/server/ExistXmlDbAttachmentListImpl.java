package org.bungeni.editor.gwt.an.server;

public class ExistXmlDbAttachmentListImpl {

	private RequestBuilder rBuilder;

	final String url = "http://127.0.0.1:8088/exist/restxq/attachments";
	
	public ExistXmlDbAttachmentListImpl() {
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
	}
		
	public ExistXmlDbAttachmentListImpl(String searchStr) {

		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
		setSearchString(searchStr);
	}
	
	public ExistXmlDbAttachmentListImpl(String searchStr, String pageStr, String strPerPage) {

		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
		
		if(!searchStr.equals(""))
			setSearchString(searchStr);
		
		if(!strPerPage.equals(""))
			setPerPage(strPerPage);	
		
		if(!pageStr.equals(""))
			setPage(pageStr);
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setSearchString(String searchStr){
		
		rBuilder.addParameter("search=".concat(searchStr));
	}
	
	public void setPage(String page){
		
		rBuilder.addParameter("page=".concat(page));
	}

	public void setPerPage(String perPage){
	
		rBuilder.addParameter("perPage=".concat(perPage));
	}
	
	public String getListAsString(){
		
		try{
			
			rBuilder.doRequest();
			String response = rBuilder.getResponse();
			System.out.println(rBuilder.toString());
			System.out.println("--Success--");
			return response;
		}
		catch(Exception e){
			
			System.out.println(e.getMessage());
			return null;
		}
	}
}
