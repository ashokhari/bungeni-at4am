package org.bungeni.editor.gwt.an.server;

public class ExistXmlDbAmendmentListImpl {

	private RequestBuilder rBuilder;

	final String url = "http://127.0.0.1:8088/exist/restxq/amendments";
	String attachmentFileName = null;
	
	public ExistXmlDbAmendmentListImpl(){
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
	}
	
	public ExistXmlDbAmendmentListImpl(String attachmentFileName){
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
		
		this.setAttachmentFileName(attachmentFileName);
	}
	
	public void setAttachmentFileName(String attachmentFileName){
		
		rBuilder.addParameter("filename=".concat(attachmentFileName));
	}
	
	public String getListAsString(){
		
		try{
			
			rBuilder.doRequest();
			String response = rBuilder.getResponse();
			System.out.println(rBuilder.toString());
			System.out.println("--Success--");
			return response;
		}
		catch(Exception e){
			
			System.out.println(e.getMessage());
			return null;
		}
	}
}
