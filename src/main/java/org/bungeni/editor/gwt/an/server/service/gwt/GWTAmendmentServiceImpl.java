/**
 * Copyright 2013 European Parliament
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package org.bungeni.editor.gwt.an.server.service.gwt;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
//import com.google.common.io.Files;
import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;

import org.bungeni.editor.gwt.an.server.ExistXmlDbAmendmentListImpl;
import org.bungeni.editor.gwt.an.server.ExistXmlDbPopAmendmentImpl;
import org.bungeni.editor.gwt.an.server.ExistXmlDbPushAmendmentImpl;
import org.bungeni.editor.gwt.an.server.RequestBuilder;
import org.nsesa.editor.gwt.an.server.service.gwt.SpringRemoteServiceServlet;
import org.nsesa.editor.gwt.core.client.service.gwt.GWTAmendmentService;
import org.nsesa.editor.gwt.core.shared.AmendableWidgetReference;
import org.nsesa.editor.gwt.core.shared.AmendmentContainerDTO;
import org.nsesa.editor.gwt.core.shared.ClientContext;
import org.nsesa.editor.gwt.core.shared.PersonDTO;
import org.nsesa.editor.gwt.core.shared.exception.ResourceNotFoundException;
import org.nsesa.editor.gwt.core.shared.exception.StaleResourceException;
import org.nsesa.editor.gwt.core.shared.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Date: 24/06/12 19:57
 *
 * @author <a href="mailto:philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
public class GWTAmendmentServiceImpl extends SpringRemoteServiceServlet implements GWTAmendmentService {

    private static final Logger LOG = LoggerFactory.getLogger(GWTAmendmentServiceImpl.class);

    private Map<String, Resource> documents;
    private Resource documentTemplate;
    private Resource amendmentDirectory;

    @Override
    public AmendmentContainerDTO getAmendmentContainer(final ClientContext clientContext, final String id) throws UnsupportedOperationException, ResourceNotFoundException {
        return null;
    }

    @Override
    public AmendmentContainerDTO[] getAmendmentContainers(final ClientContext clientContext) throws UnsupportedOperationException {

        try {
        	
        	@SuppressWarnings("static-access")
			String documentId = clientContext.getParameter(clientContext.DOCUMENT_ID)[0];
        	System.out.println(documentId);
        	
        	ExistXmlDbAmendmentListImpl amendments = new ExistXmlDbAmendmentListImpl(documentId);
    		System.out.println(amendments.getListAsString());
    		String xmlResponseString=amendments.getListAsString();
    		
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(new InputSource(new StringReader(xmlResponseString)));
    		doc.getDocumentElement().normalize();

    		XPath xpath = XPathFactory.newInstance().newXPath();
    		NodeList nList =  (NodeList) xpath.evaluate("/amendments/amendmentDocument",doc,XPathConstants.NODESET);
    		
    		AmendmentContainerDTO[] amendmentsDto = new AmendmentContainerDTO[nList.getLength()];
            
            for(int i=0; i<((NodeList) nList).getLength(); i++){
    			
            	AmendmentContainerDTO amendmentDto = new AmendmentContainerDTO();
    			Node nNode = ((NodeList) nList).item(i);
    	 
    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    	 
    				Element eElement = (Element) nNode;
    				
    				String refStr = eElement.getElementsByTagName("ref").item(0).getTextContent();
    				String name = eElement.getElementsByTagName("name").item(0).getTextContent();
    				String document = nodeToString(eElement.getElementsByTagName("akomaNtoso").item(0));
    				
    				System.out.println("Reference: "+refStr);
    				System.out.println("Document Name: "+name);
    				System.out.println("Document: \n"+document);
    				
    				//previous uuid
    				String strUuid=name.replace(refStr.concat("-"),"").replace("-am.xml","");

    				amendmentDto.setId(strUuid);
    				amendmentDto.setRevisionID(UUID.randomUUID().toString());
        			
                    AmendableWidgetReference reference = new AmendableWidgetReference(refStr);
                    amendmentDto.setSourceReference(reference);
                    
                    String content=toHTML(document.getBytes());
                    amendmentDto.setBody(content);
                    System.out.println(content);
    			}
    			
    			Arrays.fill(amendmentsDto, amendmentDto);
            }
            
            return amendmentsDto;

        } catch (IOException e) {
            throw new RuntimeException("Could not retrieve amendment content.", e);
        } catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }

    private static String nodeToString(Node node) {
    	
		StringWriter sw = new StringWriter();
		try {
			
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
		} 
		catch (TransformerException te) {
			
			System.out.println("nodeToString Transformer Exception");
		}
		
		return sw.toString();
    }

    private void validate(final String content) throws ValidationException {
        LOG.debug("====================== AN VALIDATION ======================");
        System.setProperty("javax.xml.validation.SchemaFactory:" + XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI, "org.apache.xerces.jaxp.validation.XMLSchemaFactory");

        List<Source> schemas = new ArrayList<Source>();
        schemas.add(new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("xml.xsd")));
        schemas.add(new StreamSource(Thread.currentThread().getContextClassLoader().getResourceAsStream("akomantoso20.xsd")));
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
//            factory.setValidating(true);
            factory.setNamespaceAware(true);

            SchemaFactory schemaFactory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI);

            factory.setSchema(schemaFactory.newSchema(schemas.toArray(new Source[schemas.size()])));

            SAXParser parser = factory.newSAXParser();

            XMLReader reader = parser.getXMLReader();

            final LoggingErrorHandler loggingErrorHandler = new LoggingErrorHandler();
            reader.setErrorHandler(loggingErrorHandler);
            reader.parse(new InputSource(new ByteArrayInputStream(content.trim().getBytes("UTF-8"))));

        } catch (ParserConfigurationException e) {
            throw new ValidationException("Problem parsing XML - configuration error: " + e.getMessage(), e);
        } catch (IOException e) {
            throw new ValidationException("Problem parsing XML - I/O error: " + e.getMessage(), e);
        } catch (SAXException e) {
            throw new ValidationException("Problem parsing XML - SAX error: " + e.getMessage(), e);
        } finally {
            LOG.debug("==================== END OF VALIDATION ====================");
        }

    }

    public static class LoggingErrorHandler implements ErrorHandler {
        @Override
        public void warning(SAXParseException e) throws SAXException {
            LOG.warn("SAX warning: " + e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            LOG.warn("SAX error: " + e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException {
            LOG.warn("SAX fatal: " + e);
        }
    }

    private String toHTML(byte[] bytes) {
        try {
            final InputSource inputSource = new InputSource(new ByteArrayInputStream(bytes));
            final NodeModel model = NodeModel.parse(inputSource);
            final Configuration configuration = new Configuration();
            configuration.setDefaultEncoding("UTF-8");
            configuration.setDirectoryForTemplateLoading(documentTemplate.getFile().getParentFile());
            final StringWriter sw = new StringWriter();
            Map<String, Object> root = new HashMap<String, Object>();
            root.put("doc", model);
            configuration.getTemplate(documentTemplate.getFile().getName()).process(root, sw);
            return sw.toString();

        } catch (IOException e) {
            throw new RuntimeException("Could not read file.", e);
        } catch (SAXException e) {
            throw new RuntimeException("Could not parse file.", e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("Could not parse file.", e);
        } catch (TemplateException e) {
            throw new RuntimeException("Could not load template.", e);
        }
    }

    @Override
    public AmendmentContainerDTO[] getRevisions(final ClientContext clientContext, final String id) throws UnsupportedOperationException, ResourceNotFoundException {
        return new AmendmentContainerDTO[0];
    }

    @Override
    public AmendmentContainerDTO[] saveAmendmentContainers(final ClientContext clientContext, final ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, StaleResourceException, ValidationException {
        List<AmendmentContainerDTO> amendmentContainerDTOs = new ArrayList<AmendmentContainerDTO>();
        for (AmendmentContainerDTO data : amendmentContainers) {
            //try {
            	
            	String amId=data.getId();
            	String amSrcRefPath=data.getSourceReference().getPath();
            	String amRevId=data.getRevisionID();
            	
            	System.out.println("====================================");
            	System.out.println("SAVE");
            	System.out.println("====================================");
            	System.out.println("Id: "+amId);
            	System.out.println("Source Reference Path: "+amSrcRefPath);
            	System.out.println("Revision Id: "+amRevId);
            	System.out.println("====================================");
            	
            	//To exist xmldb
                String fileName = data.getSourceReference().getPath() + "-" + data.getId() + "-am.xml";
                ExistXmlDbPushAmendmentImpl attachment = new ExistXmlDbPushAmendmentImpl("admin","");
                validate(data.getBody());
                
                //data.setId(amId);
                //data.setSourceReference(new AmendableWidgetReference(amSrcRefPath));
                //data.setRevisionID(amRevId);
                
        		attachment.setDocument(fileName.replace("#",""), data.getBody());
        		String pushResponse = attachment.push();
        		System.out.println(data.getBody());
        		
           // } catch (Exception e) {
              //  LOG.error("Could not write file.", e);
            //}
            //try {

        		//data.setBody(toHTML(data.getBody().getBytes("UTF-8")));
        		
            //} catch (Exception e) {
             //   LOG.error("Could not get encoding.", e);
           // }
            amendmentContainerDTOs.add(data);
        }
        return amendmentContainerDTOs.toArray(new AmendmentContainerDTO[amendmentContainerDTOs.size()]);
    }

    @Override
    public Boolean[] canSaveAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, StaleResourceException {
        return Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, Boolean>() {
            @Override
            public Boolean apply(AmendmentContainerDTO input) {
                return true;
            }
        }).toArray(new Boolean[amendmentContainers.size()]);
    }

    @Override
    public AmendmentContainerDTO[] deleteAmendmentContainers(final ClientContext clientContext, final ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        final Collection<AmendmentContainerDTO> deleted = Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, AmendmentContainerDTO>() {
            @Override
            public AmendmentContainerDTO apply(AmendmentContainerDTO input) {
            	
            	input.setAmendmentContainerStatus("deleted");
            	
            	String amId=input.getId();
            	String amSrcRefPath=input.getSourceReference().getPath();
            	String amRevId=input.getRevisionID();
            	
//            	System.out.println("====================================");
//            	System.out.println("DELETE");
//            	System.out.println("====================================");
//            	System.out.println("Id: "+amId);
//            	System.out.println("Source Reference Path: "+amSrcRefPath);
//            	System.out.println("Revision Id: "+amRevId);
//            	System.out.println("====================================");
            	
            	System.out.println("==========================================");
                System.out.println("Delete");
                System.out.println("==========================================");
                System.out.println("Id: "+input.getId());
                System.out.println("Path: "+input.getSourceReference().getPath());
                System.out.println("Type: "+input.getSourceReference().getType());
                System.out.println("Status: "+input.getAmendmentContainerStatus());
                System.out.println("Revision ID"+input.getRevisionID());
                System.out.println("==========================================");
            	
            	
            	String filename=amSrcRefPath.concat("-".concat(amId.concat("-am.xml")));
            	
            	ExistXmlDbPopAmendmentImpl amendment = new ExistXmlDbPopAmendmentImpl(filename);
        		amendment.setUsername("admin");
        		amendment.setPassword("*");
        		RequestBuilder rBuilder=amendment.getRequestBuilder();
        		String response=amendment.pop();
        		System.out.println(rBuilder.toString());
        		System.out.println(response);
            	
                return input;
            }
        });
        return deleted.toArray(new AmendmentContainerDTO[deleted.size()]);
    }

    @Override
    public Boolean[] canDeleteAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        return Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, Boolean>() {

            @Override
            public Boolean apply(AmendmentContainerDTO input) {
                return true;
            }
        }).toArray(new Boolean[amendmentContainers.size()]);
    }

    @Override
    public AmendmentContainerDTO[] tableAmendmentContainers(final ClientContext clientContext, final ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        final Collection<AmendmentContainerDTO> tabled = Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, AmendmentContainerDTO>() {
            @Override
            public AmendmentContainerDTO apply(AmendmentContainerDTO input) {
                input.setAmendmentContainerStatus("tabled");
                return input;
            }
        });
        return tabled.toArray(new AmendmentContainerDTO[tabled.size()]);
    }

    @Override
    public Boolean[] canTableAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        return Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, Boolean>() {

            @Override
            public Boolean apply(AmendmentContainerDTO input) {
                return true;
            }
        }).toArray(new Boolean[amendmentContainers.size()]);
    }

    @Override
    public AmendmentContainerDTO[] withdrawAmendmentContainers(final ClientContext clientContext, final ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        final Collection<AmendmentContainerDTO> withdrawn = Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, AmendmentContainerDTO>() {
            @Override
            public AmendmentContainerDTO apply(AmendmentContainerDTO input) {
                input.setAmendmentContainerStatus("withdrawn");
                return input;
            }
        });
        return withdrawn.toArray(new AmendmentContainerDTO[withdrawn.size()]);
    }

    @Override
    public Boolean[] canWithdrawAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        return Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, Boolean>() {

            @Override
            public Boolean apply(AmendmentContainerDTO input) {
                return true;
            }
        }).toArray(new Boolean[amendmentContainers.size()]);
    }

    @Override
    public AmendmentContainerDTO[] registerAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        final Collection<AmendmentContainerDTO> registered = Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, AmendmentContainerDTO>() {
            @Override
            public AmendmentContainerDTO apply(AmendmentContainerDTO input) {
                input.setAmendmentContainerStatus("registered");
                return input;
            }
        });
        return registered.toArray(new AmendmentContainerDTO[registered.size()]);
    }

    @Override
    public Boolean[] canRegisterAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        return Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, Boolean>() {

            @Override
            public Boolean apply(AmendmentContainerDTO input) {
                return true;
            }
        }).toArray(new Boolean[amendmentContainers.size()]);
    }

    @Override
    public AmendmentContainerDTO[] returnAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        final Collection<AmendmentContainerDTO> returned = Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, AmendmentContainerDTO>() {
            @Override
            public AmendmentContainerDTO apply(AmendmentContainerDTO input) {
                input.setAmendmentContainerStatus("returned");
                return input;
            }
        });
        return returned.toArray(new AmendmentContainerDTO[returned.size()]);
    }

    @Override
    public Boolean[] canReturnAmendmentContainers(ClientContext clientContext, ArrayList<AmendmentContainerDTO> amendmentContainers) throws UnsupportedOperationException, ResourceNotFoundException, StaleResourceException {
        return Collections2.transform(amendmentContainers, new Function<AmendmentContainerDTO, Boolean>() {

            @Override
            public Boolean apply(AmendmentContainerDTO input) {
                return true;
            }
        }).toArray(new Boolean[amendmentContainers.size()]);
    }

    @Override
    public ArrayList<PersonDTO> getAvailableAuthors(ClientContext clientContext, String query, int limit) {
        // TODO: this should do an ascii search for potential authors
        return new ArrayList<PersonDTO>(Arrays.asList(createPerson("1", "mep1", "MEP", "Mep1"), createPerson("2", "mep2", "MEP", "Mep2")));
    }

    private PersonDTO createPerson(String id, String username, String name, String lastName) {
        final PersonDTO person = new PersonDTO();
        person.setLastName(lastName);
        person.setName(name);
        person.setUsername(username);
        person.setId(id);
        return person;
    }

    // SPRING SETTERS -------------------------------------------


    public void setDocuments(Map<String, Resource> documents) {
        this.documents = documents;
    }

    public void setDocumentTemplate(Resource documentTemplate) {
        this.documentTemplate = documentTemplate;
    }

    public void setAmendmentDirectory(Resource amendmentDirectory) {
        this.amendmentDirectory = amendmentDirectory;
    }
}
