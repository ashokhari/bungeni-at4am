package org.bungeni.editor.gwt.an.server.service.gwt;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.bungeni.editor.gwt.an.server.OAuthBungeniConsumerImpl;

import com.google.gwt.dev.ModuleTabPanel.Session;

@SuppressWarnings("serial")
public class OAuthBungeniAuthorizeServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest request, HttpServletResponse response){ 
		
		OAuthBungeniConsumerImpl oauthClient = new OAuthBungeniConsumerImpl();
		
		String code = "";
		try {
			
			HttpSession session = request.getSession();
			code = (String) session.getAttribute("code");
			
			try{
				
				if(!code.isEmpty())
					response.sendRedirect("/test_oauth_index.html?code=".concat(code));
			}
			catch(Exception e){
				
				response.sendRedirect(oauthClient.getAuthorizationUrl());
			}
		} 
		catch (IOException ioEx) {
			// TODO Auto-generated catch block
			ioEx.printStackTrace();
		}
	}
}
