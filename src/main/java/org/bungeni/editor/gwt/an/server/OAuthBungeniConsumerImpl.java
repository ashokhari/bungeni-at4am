package org.bungeni.editor.gwt.an.server;

public class OAuthBungeniConsumerImpl {

	String authUrl = "http://127.0.0.1:8081/oauth/authorize";
	String accessTokenUrl = "http://127.0.0.1:8081/oauth/access-token";
	String apiUrlWorkSpaceSections = "http://127.0.0.1:8081/api/workspace";
	String apiUrlWorkSpaceInbox = "http://127.0.0.1:8081/api/workspace/my-documents/inbox";

	String clientId = "nsesa_editor3";
	String secretKey = "ad66a581c343c4407d9eab136347250968c65f77";
	String responseType = "code";
	
	String defaultMethod = "GET";
	
	public OAuthBungeniConsumerImpl(){}
	
	public String getAuthorizationUrl(){
		
		String newUrl = authUrl.concat("?client_id=".concat(clientId))
				.concat("&client_secret=".concat(secretKey))
			    .concat("&response_type=".concat(responseType));
		
		return newUrl;
	}
	
	public String getAccessToken(String authCode){
	
		String grantType = "authorization_code";
		
		RequestBuilder rb = new RequestBuilder();
		rb.setServiceUrl(accessTokenUrl);
		rb.setMethod(defaultMethod);
		rb.addParameter("client_id=".concat(clientId));
		rb.addParameter("grant_type=".concat(grantType));
		rb.addParameter("code=".concat(authCode));
		rb.doRequest();
		
		System.out.println("===============");
		System.out.println(rb.toString());
		System.out.println("===============");
		System.out.println(rb.getResponse());
		System.out.println("===============");
		
		return rb.getResponse();
	}
	
	public String getWorkspaceSections(String accessToken){
		
		RequestBuilder rb = new RequestBuilder();
		rb.setMethod(defaultMethod);
		rb.setServiceUrl(apiUrlWorkSpaceSections);
		rb.addHeaderProperty("Authorization", "Bearer ".concat(accessToken));
		rb.doRequest();
		
		System.out.println("===============");
		System.out.println(rb.toString());
		System.out.println("===============");
		System.out.println(rb.getResponse());
		System.out.println("===============");
		
		return rb.getResponse();
	}
	
	public String getWorkspaceFilteredInboxByStatus(String accessToken, String status){
		
		RequestBuilder rb = new RequestBuilder();
		rb.setMethod(defaultMethod);
		rb.setServiceUrl(apiUrlWorkSpaceInbox);
		rb.addHeaderProperty("Authorization", "Bearer ".concat(accessToken));
		rb.addParameter("status=".concat(status));
		rb.doRequest();
		
		System.out.println("===============");
		System.out.println(rb.toString());
		System.out.println("===============");
		System.out.println(rb.getResponse());
		System.out.println("===============");
		
		return rb.getResponse();
	}
}
