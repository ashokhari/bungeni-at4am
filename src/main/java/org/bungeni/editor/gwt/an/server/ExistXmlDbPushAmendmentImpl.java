package org.bungeni.editor.gwt.an.server;

public class ExistXmlDbPushAmendmentImpl {

	private RequestBuilder rBuilder;
	
	private String username;
	private String password;
	
	final String url = "http://localhost:8088/exist/restxq/amendment/update";
	
	String document;
	String documentName;
	
	public ExistXmlDbPushAmendmentImpl(){
		
		rBuilder = new RequestBuilder();
		rBuilder.setMethod("POST");
		rBuilder.setServiceUrl(url);
	}
	
	public ExistXmlDbPushAmendmentImpl(String username, String password){
		
		rBuilder = new RequestBuilder();
		rBuilder.setMethod("POST");
		rBuilder.setServiceUrl(url);
		
		this.username = username;
		this.password = password;
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setDocument(String documentName, String document){
		
		this.document = document;
		this.documentName = documentName;
	}
	
	public String push(){
		
		String urlParams = ""; 
		urlParams += "username=".concat(username);
		urlParams += "&password=".concat(password);
		urlParams += "&documentname=".concat(documentName);
		urlParams += "&document=".concat(document);
		
		try{
			
			rBuilder.addParameter(urlParams);
			rBuilder.doRequest();
			//rBuilder.
			System.out.println("--Sucess--");
			return rBuilder.getResponse();
		}
		catch(Exception e){
			
			System.out.println("--Failure--");
			e.printStackTrace();
			return null;
		}
	}
	
	public String toString(){
		
		String urlParams = ""; 
		urlParams += "username=".concat(username);
		urlParams += "&password=".concat(password);
		urlParams += "&documentname=".concat(documentName);
		urlParams += "&document=".concat(document);
		
		return url+"?".concat(urlParams);
	}
}

