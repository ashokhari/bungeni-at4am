package org.bungeni.editor.gwt.an.server.service.gwt;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bungeni.editor.gwt.an.server.ExistXmlDbAttachmentListImpl;

@SuppressWarnings("serial")
public class ExistXmlDbDocumentListServlet extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response)  
	{   
		String strSearch = request.getParameter("search").equals("")?"":request.getParameter("search");
		String strPage = request.getParameter("page").equals("")?"":request.getParameter("page");
		String strPerPage = request.getParameter("perPage").equals("")?"":request.getParameter("perPage");
		
		//if(!strSearch.equals("")){
	    	
	    	ExistXmlDbAttachmentListImpl list = new ExistXmlDbAttachmentListImpl(strSearch, strPage, strPerPage);
	    	
	    	String xmlDocListStr;
			try {
				
				xmlDocListStr = list.getListAsString();
				response.setContentType("application/xml");
				response.getWriter().write(xmlDocListStr);
			} 
			catch (Exception e) {
				
				e.printStackTrace();
			}
		//} 
		//else
			//try {
				
				//response.getWriter().write("");
			//} 
			//catch (IOException e) {

				//e.printStackTrace();
			//}
	}
}
